__kernel void color_exchange_cl(GeglOperation *operation,
                      void *in_buf,
                      void *out_buf,
                      gint level
                      glong n_pixels) 
{
  gint chan;
  gfloat *input = in_buf;
  gfloat *output = out_buf;

  gint pixelNum = get_global_id(0);

  input  += 4*pixelNum;
  output += 4*pixelNum;
  if ((GEGL_FLOAT_EQUAL (input[0], params->min[0]) ||
      (input[0] > params->min[0])) &&
      (GEGL_FLOAT_EQUAL (input[0], params->max[0]) ||
      (input[0] < params->max[0])) &&
      (GEGL_FLOAT_EQUAL (input[1], params->min[1]) ||
      (input[1] > params->min[1])) &&
      (GEGL_FLOAT_EQUAL (input[1], params->max[1]) ||
      (input[1] < params->max[1])) &&
      (GEGL_FLOAT_EQUAL (input[2], params->min[2]) ||
      (input[2] > params->min[2])) &&
      (GEGL_FLOAT_EQUAL (input[2], params->max[2]) ||
      (input[2] < params->max[2])))
    {
      gfloat delta[3];

      for (chan = 0; chan < 3 ; chan++)
        {
          delta[chan] = input[chan] > params->color_in[chan] ?
            input[chan] - params->color_in[chan] :
            params->color_in[chan] - input[chan];

          output[chan] = CLAMP (params->color_out[chan] + delta[chan],
                                0.0, 1.0);
        }
    }
  else
    {
      output[0] = input[0];
      output[1] = input[1];
      output[2] = input[2];
    }

  output[3] = input[3];


}
