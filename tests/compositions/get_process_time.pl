#!/usr/bin/perl
use strict;
use warnings;
use XML::Twig;
use File::Basename;
use Image::Size;
use List::Util qw[min];

# Usage
# perl get_process_time.pl [operation-file] <cpu|accelerator|gpu|no> [disable-make] [#iterations] [#var_sizes] [enable-csv]

# Arguments
my $operation    =  $ARGV[0]; # 'stretch-contrast.xml'
my $cl_device    =  $ARGV[1]; # see above
my $disable_make = !$ARGV[2]; # [0,1]
my $iterations   =  $ARGV[3]; # [0-9]+
my $varsz        =  $ARGV[4]; # [0-9]+
my $enable_csv   =  $ARGV[5]; # [0,1]


# Write to a null image so as to not bring up the gtk viewer
my $output = '/dev/null/a.png';
my $executable = '../../bin/gegl ';
my $op_name = fileparse($operation, '\..*');

# Read XML from file
my $twig = new XML::Twig;
$twig->parsefile($operation);

# Get width and height elements in XML
my $xpath_str_crop = '/gegl/node[@operation="gegl:crop"]/params/param[@name="width" or @name="height"]';
my $xpath_str_img = '/gegl/node[@operation="gegl:load"]/params/param[@name="path"]';
my @crop_wh = $twig->find_nodes($xpath_str_crop);
my @img_path = $twig->find_nodes($xpath_str_img);
my @img_size = imgsize($img_path[0]->text);

# Enable variable img sizes
if ($varsz) {
    $crop_wh[0]->set_text(32);
    $crop_wh[1]->set_text(32);
}
my $op_xml = $twig->root->sprint;

# If no cl_device is specified, assume OpenCL is disabled
$ENV{"GEGL_DEBUG_TIME"} = 1;
$ENV{"GEGL_USE_OPENCL"} = $cl_device;

# Make the source tree unless explicitly disabled
if ($disable_make) {
    system("make", "-C", "../../");
}

if ($varsz) {
    for (my $i = 0; $i < $varsz && ($crop_wh[0]->text <= $img_size[0]) && ($crop_wh[1]->text <= $img_size[1]); ++$i) {
        if ($enable_csv) {
            print $crop_wh[0]->text . "x" . $crop_wh[1]->text . ", ";
        } else {
            print "Image size is " . $crop_wh[0]->text . "x" . $crop_wh[1]->text . "\n";
        }
        #$output = "~/$i-out.png"; #DEBUG
        $op_xml = $twig->root->sprint;
        my ($avg_tot_time, $avg_process_time, $avg_op_time) = get_avg_time();
        # up the crop area for next iteration
        $crop_wh[0]->set_text(min($crop_wh[0]->text * 2, $img_size[0]));
        $crop_wh[1]->set_text(min($crop_wh[1]->text * 2, $img_size[1]));
        #print $op_xml; #DEBUG
    }
} else {
    get_avg_time();
}



# Process GEGL_DEBUG_TIME output for stats of interest
sub parse_debug_time {
    my $tot_time = 0;
    my $process_time = 0;
    my $op_time = 0;
    while (my $line = shift @_) {
        #print $line; #DEBUG

        # Regex that captures float variables
        my $float_var = qr/[0-9]+\.[0-9]+/;

        # Capture total time, process %, and operation %
        if ($line =~ /(${float_var})s/) {
            $tot_time = $1;
        } elsif ($line =~ /process\s+(${float_var})%/) {
            $process_time = $tot_time*$1/100;
        } elsif ($line =~ /$op_name\s+(${float_var})%/) {
            $op_time = $process_time*$1/100;
            last;
        }
    }
    #print "Intermediate Total = ", $tot_time, "s\n";
    #print "Intermediate Process = ", $process_time, "s\n";
    #print "Intermediate Operation = ", $op_time, "s\n"; #DEBUG

    return ($tot_time, $process_time, $op_time);
}

sub get_avg_time {
    my $avg_tot_time = 0;
    my $avg_process_time = 0;
    my $avg_op_time = 0;
    for(my $i = 0; $i < $iterations; ++$i) {
        my @timing = `$executable -x '$op_xml' -o $output`;
        my ($tot_time, $process_time, $op_time) = parse_debug_time(@timing);
        $avg_tot_time       += $tot_time;
        $avg_process_time   += $process_time;
        $avg_op_time        += $op_time;
    }

    if ($enable_csv) {
        print $avg_op_time/$iterations, "\n";
    } else {
        #print "Average Time Breakdown:\n";
        #print "Total = ", $avg_tot_time/$iterations, "s\n";
        #print "Process = ", $avg_process_time/$iterations, "s\n";
        print "Operation = ", $avg_op_time/$iterations, "s\n";
    }

    return ($avg_tot_time, $avg_process_time, $avg_op_time);
}
