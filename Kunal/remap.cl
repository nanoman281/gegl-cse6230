_kernel void gboolean_cl (GeglOperation        *op,
                             void                 *in_buf,
                             void                 *min_buf,
                             void                 *max_buf,
                             void                 *out_buf,
                             glong                n_pixels,
                             const GeglRectangle  *roi,
                             gint                 level)
{
  int gidx = get_global_id(0);

  gint i;
  gfloat *in = in_buf;
  gfloat *min = min_buf;
  gfloat *max = max_buf;
  gfloat *out = out_buf;


  gfloat delta1 = max[0] - min[0];
  gfloat delta2 = max[1] - min[1];
  gfloat delta3 = max[2] - min[2];


  if(delta1 > 0.0001 || delta1 < -0.0001)
    out[0] = (in[0] - min[0]) / delta1;
  else
    out[0] = in[0];


  if(delta2 > 0.0001 || delta2 < -0.0001)
    out[1] = (in[1] - min[1]) / delta2;
  else
    out[1] = in[1];


  if(delta3 > 0.0001 || delta3 < -0.0001)
    out[2] = (in[2] - min[2]) / delta3;
  else
    out[2] = in[2];

  out[3] = in[3];


  in += gidx*4;
  out += gidx*4;
  min += gidx*4;
  max += gidx*4;
}


