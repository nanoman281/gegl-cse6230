_kernel void process_cl(GeglOperation        *operation,
                        GeglBuffer           *input,
                        GeglBuffer           *output,
                        const GeglRectangle  *result,
                        gint                 level) 
{
  const GeglRectangle *whole_region;
  GeglRectangle       shift_region;
  GeglBufferIterator  *gi;
  gint                half_width;
  gint                half_height;
  gint                index_iter;
  gint                index_iter2;

  whole_region = gegl_operation_source_get_bounding_box(operation, "input");

  half_width = whole_region -> width/2;
  half_height = whole_region -> height/2;

  shift_region.x = whole_region -> x + half_width;
  shift_region.y = whole_region -> y + half_height;
  shift_region.width = whole_region -> width;
  shift_region.height = whole_region -> height;

  //do we need to combine all the iterators?

  int gidx = get_global_id(0);


  guint k;
  gfloat *data_out;
  gfloat *data_in1;
  gfloat *data_in2;

  data_out = (gfloat*) gi -> data[0];
  data_in1 = (gfloat*) gi -> data[index_iter];
  data_in2 = (gfloat*) gi -> data[index_iter2];

  for(k = 0; k < gi -> length; k++) {
    gint x, y, b;
    gfloat alpha;
    gfloat val_x, val_y;
    gfloat w, w1, w2;
    const gfloat eps = 1e-4;

    x = gi -> roi[0].x + k % gi -> roi[0].width;
    y = gi -> roi[0].y + k % gi -> roi[0].width;

    val_x = (half_width - x) / (gfloat) half_width;
    val_y = (half_height - y) / (gfloat) half_height;

    val_x = ABS (CLAMP (val_x, -1.0, 1.0));
    val_y = ABS (CLAMP (val_y, -1.0, 1.0));

    if(ABS (val_x - val_y) >= 1.0 - eps)
      w = 0.0;
    else
      w = val_x * val_y / (val_x * val_y + (1.0 - val_x) * (1.0 - val_y));

    alpha = data_in[3] * (1.0 - w) + data_in2[3] * w;

    w1 = (1.0 - w) * data_in1[3] / alpha;
    w2 = w * data_in2[3] / alpha.


    data_out[0] = data_in1[0] * w1 + data_in2[0] * w2;
    data_out[1] = data_in1[1] * w1 + data_in2[1] * w2;
    data_out[2] = data_in1[2] * w1 + data_in2[2] * w2;

    data_out[3] = alpha;

    data_out += gidx*4;
    data_in1 += gidx*4;
    data_in2 += gidx*4;

  }

 
}
